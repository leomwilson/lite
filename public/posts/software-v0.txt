Software List (v0)
Leo Wilson - 20 Jul 2020 @ 15:43
Originally posted on 28 May 2020 at 19:38 to https://leomwilson.com/posts/software-v0

Here's a non-exhaustive list of some notable programs and websites that I personally use on a daily basis. This page won't be updated, but I'll try to publish new lists occasionally.

## OS
I'm currently running Qubes OS on my main computer, though I plan to switch back to Arch Linux at some point in the future.

## Web Browser
Because of its freedom and openness, I use Firefox on both my computer and phone. Usually, I also keep Chromium for the rare instances when I need it. I also have TOR Browser for anything particularly sensitive.

I have a number of extensions:

* Bitwarden
* ClearURLs
* Dark Reader
* Decentraleyes
* Facebook Container
* HTTPS Everywhere
* Mailvelope
* New Tab Override (I have a custom homepage at https://leomwilson.com/startpage)
* OverbiteWX
* Privacy Badger
* Stylus
* Terms of Service; Didn't Read
* uBlock Origin
* User-Agent Switcher and Manager

## Communication
I generally use Signal for texting, preferably encrypted but often over basic SMS. For obvious reasons, my phone number isn't public.

For email, I use ProtonMail (through its webmail interface), which I highly recommend. I also have a Gmail account that I use for some old and unimportant accounts, but I'm in the process of transitioning to PM entirely.

Mastodon is my primary form of traditional social media, but I also have an Instagram account, which I plan to delete soon. On my phone, I access Mastodon via Tusky.

I also use Discord for a few servers, such as the one for my FRC Team.

## Media
Reddit, which I access through Sync Pro, is my source for memes and technology-related news. For general news, I use an aggregator called Feedly.

My preferred source of video content is LBRY, but since only a few creators upload there, I also use Invidious (with youtube-dl), which provides a frontend for content uploaded to YouTube. On my phone, I use NewPipe for the same purpose. Unlike YouTube's app or website, these respect privacy. I also have Netflix, Prime Video, and Patreon.

For music, I currently use Amazon Music. I plan to look into alternatives once my current subscription ends.

## Capitalism
Leo Merch is hosted on Teespring.

For receiving payments I use PayPal and Ko-Fi, and I use the "standard" apps for my cryptocurrencies (e.g. Bitcoin Core).

Most of my shopping is done on Amazon.

## Code
Right now, I'm typing this into VSCode. Soon, I'll sign the commit and push it to GitLab, where I store most of my personal projects. I have a GitHub profile, but I only use it for contributing to projects or organizations that haven't yet moved to GitLab.

## Website
This website is hosted on Netlify (which I recommend for anyone looking for free static hosting) and pulled directly from the GitLab repo. The domain name is registered through Hover. If it had a backend, it would likely be hosted on Heroku with an AWS DynamoDB database.

The website itself is built with Hugo and uses the Hermin theme, my fork of Hermit.

## Security
OpenPGP encryption is built into ProtonMail, but for other OpenPGP tasks, I use GnuPG. I don't use anything special for WKD (I manually copy the file there), and I pretty much push to whatever major keyservers I can find. As I mentioned previously, I almost always sign my Git commits.

While I have my reservations about Keybase being acquired by Zoom, I still have a profile there for cryptographic proofs of some of my accounts. I don't use their other features, like chat and file sharing.

My password manager is Bitwarden, and I'd encourage everyone currently using something else to try it.

I use andOTP for 2FA. If you're using Google/Microsoft Authenticator, you might want to consider it.

## Misc Free Software
I use F-Droid instead of the Google Play Store whenever possible. I recently switched from Gboard to the privacy-focused AnySoftKeyboard.
