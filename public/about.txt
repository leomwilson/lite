About Me

I am currently the Lead Programmer and the Technical Director for FRC team 6479.
I regularly work with Java, Rust, and TypeScript, with the occasional bit of Python.

Outside of programming, my hobbies are 3D printing and mechanical keyboards.
Additionally, as a supporter and user of Free/Libre and Open Source Software, I use Arch (btw).

This website is a plaintext version of my main site at https://leomwilson.com.
You can find the source "code" to this site on GitLab at https://gitlab.com/leomwilson/lite.

This website and its contents are licensed under Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0