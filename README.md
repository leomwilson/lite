# lite.leomwilson.com

A significantly lighter version of my website. See also [leomwilson/com](https://gitlab.com/leomwilson/com).

This is not intended to be a fully-featured replacement, just an option for viewing most of the essentials of my website in plaintext.

As a simple hack for clean URLs, files are moved to their own directory, of which they are the index, such that `/about.txt` is moved to `/about/index.txt`.
`public/_redirects` make this work for .txt files on Netlify.

Everything is stored in `public/`. To serve the website, configure your server of choice to use it as the website's root directory.

This version of the website is entirely static, meaning that, like its big brother, it can be hosted on Netlify.
However, while the main site uses Hugo, this doesn't require any processing or generator programs.

## FAQ

### Why is *feature or page* not here?
Because I either haven't gotten to it or, more likely, didn't think it was important/well-suited enough to be added here.

### How often will posts be synced?
Currently, there's no automated way for me to sync posts, so I have to manually copy and paste them.
I'll do that whenever I feel like it, so expect this site to be a bit behind.

### I see formatting!
I'll occasionally add Markdown-esque "formatting" if I feel like it would improve the experience.
This site is still intended to be viewed as plaintext.
It happens that most people who would look at this site understand Markdown,
so using a # for a header would intuitively make sense.

Additionally, some "formatting" characters aren't really intended as formatting.
For example, using asterices is a common convention outside of Markdown to denote action.

However, you will never see HTML here because HTML is bloat.

### Is this in any standard, machine-readable format?
No, it is designed for humans. That being said, I ~~do~~ will have an RSS feed for my posts.

### Why not Gopher?
If you feel like paying for a VPS to host it, I'll be happy to. For now, Netlify gang.

### yOuR sItE iS uGlY
Yes.

### Why does this exist?
As a quiet rebellion against modernity and bloat.